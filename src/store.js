import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    contacts: [],
    chats: {},
    activeChatId: null
  },
  mutations: {
    loadContacts(state, payload) {
      state.contacts = payload;
      return state;
    },
    sendMessage(state, payload) {
      let chatId = this.state.activeChatId;
      if (state.chats[chatId] === undefined) {
        Vue.set(state.chats, chatId, []);
      }

      state.chats[chatId].push({
        id: Date.now(),
        own: true,
        message: payload.message
      });
      setTimeout(() => {
        this.commit("recieveMessage", {
          chatId: chatId,
          message: payload.message
        });
      }, 1000);
      return state;
    },
    recieveMessage(state, payload) {
      state.chats[payload.chatId].push({
        id: Date.now(),
        own: false,
        message: payload.message
      });
    },
    openChat(state, payload) {
      state.activeChatId = payload.id;
    }
  },
  actions: {
    fetchData({ commit }) {
      axios.get("https://jsonplaceholder.typicode.com/users").then(res => {
        commit("loadContacts", res.data);
        commit("openChat", this.state.contacts[0]);
      });
    },
    sendMessage({ commit }, message) {
      commit("sendMessage", message);
    }
  },
  methods: {
    getRandomColor: function() {
      return "#" + Math.floor(Math.random() * 16777215).toString(16);
    }
  }
});
